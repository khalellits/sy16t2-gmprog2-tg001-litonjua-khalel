﻿using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour
{
    public RigidbodyInterpolation2D interpolation;
    public GameObject player;
    private Vector3 offset;
	private float CamScale;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        offset = transform.position - player.transform.position;
    }

    void Update ()
	{
		transform.position = player.transform.position + offset;

		if (player.transform.localScale.x > CamScale) {
			Camera.main.orthographicSize += .90f;
			CamScale = player.transform.localScale.x;
		}
	}
}