﻿using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour {

	public GameObject[] Enemies;
	public int Amount;
	private Vector3 SpawnPoint;

	void Update () {
		Enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		Amount = Enemies.Length;

		if (Amount != 5) 
		{
			InvokeRepeating ("spawnEnemy", 1, 2.5f);
		}
	}
	void spawnEnemy ()
	{
		SpawnPoint.x = Random.Range(-5, 5);
		SpawnPoint.y = Random.Range(-5, 5);
		SpawnPoint.z = Random.Range(-5, 5);

		Instantiate (Enemies [UnityEngine.Random.Range (0, Enemies.Length - 1)], SpawnPoint, Quaternion.identity);
		CancelInvoke ();
	}

}
