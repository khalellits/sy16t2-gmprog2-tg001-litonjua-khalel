﻿using UnityEngine;
using System.Collections;

public class AdjustScale : MonoBehaviour    {

    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            gameObject.transform.localScale += new Vector3(0.1f, 0.1f, 0);
        }

        else if (Input.GetKey(KeyCode.W))
        {
            gameObject.transform.localScale -= new Vector3(0.1f, 0.1f, 0);
        }
    }
}