﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

	public GameObject[] Foods;
	public int Amount;
	private Vector3 SpawnPoint;

	void Start()
	{
		for (int x = 0; x < 30; x++)
		{
			spawnFood();
		}
	}

	void Update () {
		Foods = GameObject.FindGameObjectsWithTag ("Food");
		Amount = Foods.Length;

		if (Amount != 100) 
		{
			InvokeRepeating ("spawnFood", 1, 2.5f);
		}
	}
	void spawnFood ()
	{
		SpawnPoint.x = Random.Range(-20, 20);
		SpawnPoint.y = Random.Range(-5, 5);
		SpawnPoint.z = Random.Range(-20, 20);

		Instantiate (Foods [UnityEngine.Random.Range (0, Foods.Length - 1)], SpawnPoint, Quaternion.identity);
		CancelInvoke ();
	}

}
