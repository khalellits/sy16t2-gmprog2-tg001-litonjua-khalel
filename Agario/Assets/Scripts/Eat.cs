﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Eat : MonoBehaviour {
	
	public GameObject Enemy;
	public GameObject Food;
	public Text Letters;
	public float Increase;

	int Score = 0;

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject)
		{
			transform.localScale += new Vector3(Increase, Increase, Increase);
			Destroy(other.gameObject);

			Score += 1;
			Letters.text = "SCORE: " + Score;
		}
	}
}