﻿using UnityEngine;
using System.Collections;

public class Moving : MonoBehaviour {

    
    public float Speed;

	void Update () {
        Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Target.z = transform.position.z;

        transform.position = Vector3.MoveTowards(transform.position, Target, Speed * Time.deltaTime / transform.localScale.x);

		this.transform.position = new Vector3 
			(Mathf.Clamp (this.transform.position.x, -20f, 20f),
				Mathf.Clamp (this.transform.position.y, -20f, 20f),
				0);
	}
}
