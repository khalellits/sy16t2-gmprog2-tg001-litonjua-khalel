﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	Transform target;
	Transform enemyTransform;
	public float speed;

	void Start()
	{
		enemyTransform = this.GetComponent<Transform> ();
	}

	void Update ()
	{
		target = GameObject.FindWithTag ("Player").transform;
		Vector3 targetHeading = target.position - transform.position;
		Vector3 targetDirection = targetHeading.normalized;

		transform.position = Vector3.MoveTowards (transform.position, target.transform.position, speed * Time.deltaTime);
	}
}