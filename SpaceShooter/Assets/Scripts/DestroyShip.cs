﻿using UnityEngine;
using System.Collections;

public class DestroyShip : MonoBehaviour
{

    public float HitstoDes;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Asteroid")
        {
            HitstoDes--;

            if (HitstoDes <= 0.0f)
                Destroy(gameObject);
        }
    }
}
