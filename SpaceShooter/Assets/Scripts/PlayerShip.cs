﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerShip : MonoBehaviour
{
    public float MoveSpeed = 1.0f;
    public int Life = 3;

    [Header("UI")]
    public Text LifeText;
    public Text KillCountText;

    public GameObject bullet;
    public float bulletSpawnPosition = 1.0f;
    public float bulletSpeed = 1000.0f;

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject instance = Instantiate(bullet, transform.position + transform.forward * bulletSpawnPosition, Quaternion.identity) as GameObject;
            instance.GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed);
        }

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * MoveSpeed * Time.deltaTime;

        LifeText.text = "Life: " + Life;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Asteroid") return;

        // Deduct life
        Life--;
        //if (Life <= 0) Destroy(gameObject);

        // Destroy the asteroid
        Destroy(other.gameObject);
    }
}
