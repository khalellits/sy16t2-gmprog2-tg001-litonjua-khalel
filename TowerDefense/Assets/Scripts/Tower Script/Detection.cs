﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour
{
	public GameObject target;
	public GameObject[] enemies;
	public float range = 50.0f;
	private bool onRange = false;

	void Update ()
	{
		EnemyDist ();
	}

	void EnemyDist ()
	{
		enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		int closestIndex = 0;
		float nearestDistance = Mathf.Infinity;
		float closestEnemy;
		for (int i = 0; i < enemies.Length; i++)
        {
            onRange = Vector3.Distance (transform.position, enemies [i].transform.position) < range;
			closestEnemy = Vector3.Distance (transform.position, base.transform.position);

			if (closestEnemy < nearestDistance && onRange)
            {
				nearestDistance = closestEnemy;
				closestIndex = i;
				target = enemies [i];

                transform.LookAt(target.transform);
			}
            
		}
	}
}
