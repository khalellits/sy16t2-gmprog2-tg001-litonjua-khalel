﻿using UnityEngine;
using System.Collections;

public class BuildTower : MonoBehaviour
{
    public GameObject TowerPrefab;
    private GameObject Tower;
    private GameManager gameManager;

	// Use this for initialization
	void Start () 
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    void OnMouseUp()
    {

        if (PlaceTower())
        {
            Tower = (GameObject)Instantiate(TowerPrefab, transform.position, Quaternion.identity);
            gameManager.Gold -= Tower.GetComponent<TowerData>().CurrentLevel.cost;
        }
        else if (UpgradeTower())
        {
            Tower.GetComponent<TowerData>().increaseLevel();
            gameManager.Gold -= Tower.GetComponent<TowerData>().CurrentLevel.cost;
        }
    }
    private bool PlaceTower()
    {
        
       
         int cost = TowerPrefab.GetComponent<TowerData>().levels[0].cost;
       
         return Tower == null && gameManager.Gold >= cost;

    }

    private bool UpgradeTower()
    {
        if(Tower!= null)
        {
            TowerData towerData = Tower.GetComponent<TowerData>();
            TowerLevel nextLevel = towerData.NextLevel();
            if (nextLevel != null)
            {
                return gameManager.Gold >= nextLevel.cost;
            }
        }
        return false;
    }
}
