﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Bullet : MonoBehaviour 
{
    private float distance;
    private float startTime;
    public int Damage;
    private GameManager gameManager;

	// Use this for initialization
	void Start () 
    {
        GameObject gm = GameObject.Find("GameManager");
        gameManager = gm.GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, 100f))
        {
            if (hit.transform.tag == "Enemy")
            {
                hit.transform.GetComponent<EnemyHealthScript>().Hit(1);
                Debug.Log("Hit Enemy");
            }
        }
	}

    void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag =="Enemy")
        {
            Destroy(this.gameObject);
        }
    }
}
