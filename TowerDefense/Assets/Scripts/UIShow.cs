﻿using UnityEngine;
using System.Collections;

public class UIShow : MonoBehaviour {

    public GameObject panel;

	// Use this for initialization
	void Start () {
        panel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))
            panel.SetActive(false);
	}
  
    void OnMouseDown()
    {
        panel.SetActive(true);
    }
    

 
}
