﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHealthScript : MonoBehaviour 
{
    public int Health = 100;
    public int MaxHealth = 100;
    private Image HealthBar;
    public int health { get { return Health;} }

    private GameManager gameManager;

    void Start()
    { 
        GameObject gm = GameObject.Find("GameManager");
        gameManager = gm.GetComponent<GameManager>();
    }

    public void Hit(int Damage)
    {
        Health -= Damage;

        if(Health <= 0)
        {
            Destroy(this.gameObject);

            gameManager.Gold += 50;
        }
    }
}
