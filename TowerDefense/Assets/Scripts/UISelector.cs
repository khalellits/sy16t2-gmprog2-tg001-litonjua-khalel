﻿using UnityEngine;
using System.Collections;

public class UISelector : MonoBehaviour {
    public GameObject TowerPrefab;
    private GameObject Tower;
    private GameManager gameManager;

   
	// Use this for initialization
	void Start () {
       gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CreateTower(GameObject btn)
    {
        GameObject go = Instantiate<GameObject>(TowerPrefab);
        OnMouseDown();
    }
    void OnMouseDown()
    {

        if (PlaceTower())
        {
            Tower = (GameObject)Instantiate(TowerPrefab, transform.position, Quaternion.identity);
            gameManager.Gold -= Tower.GetComponent<TowerData>().CurrentLevel.cost;
        }
        
    }
   
    private bool PlaceTower()
    {
        int cost = TowerPrefab.GetComponent<TowerData>().levels[0].cost;

        return Tower == null && gameManager.Gold >= cost;

    }


}
