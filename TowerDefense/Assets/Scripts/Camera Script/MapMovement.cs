﻿using UnityEngine;
using System.Collections;

public class MapMovement : MonoBehaviour 
{
	void Update()
		{
		
		if (Input.GetKey (KeyCode.RightArrow))
		{
			this.transform.position += new Vector3 (20 * Time.deltaTime, 0, 0);
		}

		if (Input.GetKey (KeyCode.LeftArrow))
		{
			this.transform.position += new Vector3 (-20 * Time.deltaTime, 0, 0);
		}

		if (Input.GetKey (KeyCode.UpArrow))
		{
			this.transform.position += new Vector3 (0, 0, 20 * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.DownArrow))
		{
			this.transform.position += new Vector3 (0, 0, -20 * Time.deltaTime);
		}
	}
}